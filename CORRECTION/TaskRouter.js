const express = require( "express" );
const router = express.Router();
const { TaskRegistry } = require( "./Registry" );
const { Task } = require( "./Task" );

router.get( "/", (req, res) => {

    res.status( 200 ).render( "task/NavPage", {
        list: TaskRegistry.all()
    });

});

let filter = (req, res, next) => {

    let idStr = req.params.id;
    if( idStr === null ){
        res.status( 404 ).send( "Bad request" );
        return;
    }

    let id = parseInt( idStr );
    let it = TaskRegistry.find( id );

    if( it ){
        req.task = {
            id: id,
            value: it.item
        };
        next();
    } else {
        res.status( 404 ).send( `There is no task binded to ${id} id` );
    }

};

let formFilter = (req, res, next) => {

    req.form = {
        name: req.body.name,
        description: req.body.description,
        start: req.body.start,
        duetime: req.body.duetime
    };

    if( req.form.start ){
        req.form.start = new Date( req.form.start ).getTime();
    }

    if( req.form.duetime ){
        req.form.duetime = new Date( req.form.duetime ).getTime();
    }

    next();
};

router.get( "/display/:id", filter, (req, res) => {
    console.log( req.task.value.toString() );
    res.status( 200 ).send( req.task.value.toString() );
});

router.get( "/edit/:id", filter, (req, res) => {

    res.status( 200 ).render( "task/form", {
        target: `/edit/${ req.task.id }`,
        task: req.task.value
    });

});

router.post( "/edit/:id", filter, formFilter, (req, res) => {

    if( req.form.name ){
        req.task.value.name = req.form.name;
    }

    if( req.form.description ){
        req.task.value.description = req.form.description;
    }

    if( req.form.start ){
        req.task.value.start = req.form.start;
    }

    if( req.form.duetime ){
        req.task.value.duetime = req.form.duetime;
    }

    res.status( 200 ).send( req.task.value.toString() );
});

router.post( "/delete/:id", filter, formFilter, (req, res) => {

    TaskRegistry.delete( req.task.id );
    
    res.status( 200 ).render( "task/NavPage", {
        list: TaskRegistry.all()
    });

});

router.get( "/new", (req, res) => {
    res.status( 200 ).render( "task/form", {
        target: `/task/new`
    });
});

router.post( "/new", formFilter, (req, res) => {
    
    console.log( JSON.stringify( req.form ) );

    let task = new Task(
        req.form.name,
        req.form.description,
        req.form.start,
        req.form.duetime
    );

    console.log( "Added: " + task.toString() );

    TaskRegistry.insert( task );
    res.status( 200 ).render( "task/NavPage", {
        list: TaskRegistry.all()
    });
});

module.exports = router;