const express = require( "express" );
const router = express.Router();
const { TaskListRegistry, TaskRegistry } = require( "./Registry" );
const { TaskList } = require( "./TaskList" );

let filter = (req, res, next) => {

    let idStr = req.params.id;
    if( idStr === null ){
        res.status( 404 ).send( "Bad request" );
        return;
    }

    let id = parseInt( idStr );
    let it = TaskListRegistry.find( id );

    if( it ){
        req.tasklist = {
            id: id,
            value: it.item
        };
        next();
    } else {
        res.status( 404 ).send( `There is no task list binded to ${id} id` );
    }

};

let formFilter = (req, res, next) => {

    req.form = {
        name: req.body.name,
        tasks: req.body.task || []
    };

    if( req.form.tasks ){
        if( req.form.tasks instanceof Array ){
            let t = [];

            for (const id of req.form.tasks) {
                t.push( parseInt( id ) );
            }
            
            req.form.tasks = t;
        } else {
            req.form.tasks = [
                parseInt( req.form.tasks )
            ];
        }
    }

    next();
};

router.get( "/", (req, res) => {
    res.status( 200 ).render( "list/naviguation", {
        list: TaskListRegistry.all()
    });
});

router.get( "/new", (req, res) => {

    res.status( 200 ).render( "list/form", {
        target: "/list/new",
        tasks: TaskRegistry.all(),
        selected: () => false
    });

});

router.post( "/new", formFilter, (req, res) => {

    let tasklist = new TaskList( req.form.name, req.form.tasks );
    TaskListRegistry.insert( tasklist );

    res.status( 200 ).render( "list/naviguation", {
        list: TaskListRegistry.all()
    });

});

router.get( "/:id", filter, (req, res) => {

    let tasks = [];
    for (const id of req.tasklist.value.list ) {
        tasks.push( TaskRegistry.find( id ) );
    }

    res.status( 200 ).render( "list/view", {
        tasks: tasks,
        tasklist: req.tasklist
    });
});

router.get( "/:id/edit", filter, (req, res) => {
    res.status( 200 ).render( "list/form", {
        target: `/list/${ req.tasklist.id }/edit`,
        tasks: TaskRegistry.all(),
        selected: (id) => {
            return req.tasklist.value.has( id );
        }
    });
});

router.post( "/:id/edit", filter, formFilter, (req, res) => {

    req.tasklist.value.list = req.form.tasks;

    let tasks = [];
    for (const id of req.tasklist.value.list ) {
        tasks.push( TaskRegistry.find( id ) );
    }

    res.status( 200 ).render( "list/view", {
        tasks: tasks,
        tasklist: req.tasklist
    });

});

router.post( "/delete/:id", filter, (req, res) => {
    TaskListRegistry.delete( req.tasklist.id );
    res.status( 200 ).render( "list/naviguation", {
        list: TaskListRegistry.all()
    });
});

module.exports = router;